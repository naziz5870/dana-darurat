<?php
session_start();
include 'koneksi.php';

$sql = "SELECT * FROM paket_pinjaman INNER JOIN datapeminjam ON paket_pinjaman.id_paket=datapeminjam.id_paket";
$query = mysqli_query($conn, $sql);
?>

<!DOCTYPE html>
<html lang="en">

<?php if ($_SESSION['status'] == "user") { ?>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Halaman Datapeminjam</title>
       </head>

       <body>
              <header id="header">
                     <?php
                     include('menu.php');
                     ?>
              </header>

              <h1>Datapeminjam </h1>
              <table cellspacing="0" border="1">
                     <tr>
                            <th>Nama Peminjam</th>
                            <th>Gender</th>
                            <th>Umur</th>
                            <th>Alamat</th>
                            <th>Email</th>
                            <th>Nama Paket</th>
                            <th>Jumlah Pinjaman</th>
                            <th>Bunga Paket</th>
                            <th>Cicilan Paket</th>
                            <th>Keterangan</th>
                            <th>Tanggal Meminjam</th>
                     </tr>
                     <?php
                     while ($data = mysqli_fetch_array($query)) {
                     ?>
                            <tr>
                                   <td><?php echo $data['nama_peminjam'] ?></td>
                                   <td><?php echo $data['gender_peminjam'] ?></td>
                                   <td><?php echo $data['umur_peminjam'] ?></td>
                                   <td><?php echo $data['alamat_peminjam'] ?></td>
                                   <td><?php echo $data['email'] ?></td>
                                   <td><?php echo $data['nama_paket'] ?></td>
                                   <td><strong>Rp. </strong><?php echo $data['jumlah_pinjaman'] ?></td>
                                   <td><?php echo $data['bunga_paket'] ?></td>
                                   <td><?php echo $data['cicilan_paket'] ?></td>
                                   <td class="text-success"><?php echo $data['keterangan'] ?></td>
                                   <td><?php echo $data['tanggal_meminjam'] ?></td>
                            </tr>
                     <?php
                     }
                     ?>
              </table>
       <?php } ?>
       </body>

</html>