<?php
session_start();
?>

<?php

if ($_SESSION['status'] == "") {
       header("location:index.php?pesan=gagal");
}
?>

<!DOCTYPE html>
<html lang="en">

<?php if ($_SESSION['status'] == "user") { ?>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Halaman Pengajuan Pinjaman</title>
       </head>

       <body>
              <header id="header">
                     <?php
                     include('menu.php');
                     ?>
              </header>

              <h2>DanaDarurat Kini Menawarkan Beberapa Paket Pinjaman</h2>
              <?php

              include('koneksi.php');

              $query = mysqli_query($conn, 'SELECT * FROM paket_pinjaman');
              $result = mysqli_fetch_all($query, MYSQLI_ASSOC);

              ?>

              <?php foreach ($result as $result) : ?>
                     <div class="card-body">
                            <h5 class="card-title text-center"><?php echo $result['nama_paket'] ?></h5>
                            <h4 class="card-title text-center"><strong>Rp. </strong><?php echo $result['jumlah_pinjaman'] ?></h4>
                            <p class="card-text"><strong>Bunga </strong><?php echo $result['bunga_paket'] ?></p>
                            <p class="card-text"><strong>Cicilan </strong><?php echo $result['cicilan_paket'] ?></p>
                            <a href="ajukan.php?id_paket=<?php echo $result['id_paket'] ?>">Ajukan Pinjaman</a>
                            <input type="hidden" name="id_paket" id="paket" value="id_paket">
                     <?php endforeach; ?>
       </body>
<?php } ?>

</html>