<?php
session_start();
include 'koneksi.php';

$sql = "SELECT * FROM paket_pinjaman INNER JOIN datapeminjam ON paket_pinjaman.id_paket=datapeminjam.id_paket";
$query = mysqli_query($conn, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<?php if ($_SESSION['status'] == "admin") { ?>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Halaman Data User</title>
       </head>

       <body>
              <header id="header">
                     <?php
                     include('menu1.php');
                     ?>
              </header>

              <h3>Data Peminjam</h3>
              <table cellspacing="0" border="1">
                     <tr>
                            <th>Nama Peminjam</th>
                            <th>Gender</th>
                            <th>Umur</th>
                            <th>Alamat</th>
                            <th>Email</th>
                            <th>Nama Paket</th>
                            <th>Jumlah Pinjaman</th>
                            <th>Bunga Paket</th>
                            <th>Cicilan Paket</th>
                            <th>Keterangan</th>
                            <th>Tanggal Meminjam</th>
                            <th>Aksi</th>
                     </tr>
                     <?php
                     while ($data = mysqli_fetch_array($query)) {
                     ?>
                            <tr>
                                   <td><?php echo $data['nama_peminjam'] ?></td>
                                   <td><?php echo $data['gender_peminjam'] ?></td>
                                   <td><?php echo $data['umur_peminjam'] ?></td>
                                   <td><?php echo $data['alamat_peminjam'] ?></td>
                                   <td><?php echo $data['email'] ?></td>
                                   <td><?php echo $data['nama_paket'] ?></td>
                                   <td><strong>Rp. </strong><?php echo $data['jumlah_pinjaman'] ?></td>
                                   <td><?php echo $data['bunga_paket'] ?></td>
                                   <td><?php echo $data['cicilan_paket'] ?></td>
                                   <td class="text-success"><?php echo $data['keterangan'] ?></td>
                                   <td><?php echo $data['tanggal_meminjam'] ?></td>
                                   <td><a href="editdatapeminjam.php?id=<?= $data['id_data'] ?>">Edit</a>|<a href="hapus_data.php?id_data=<?= $data['id_data'] ?>">Hapus</a></td>
                            </tr>
                     <?php
                     }
                     ?>
              </table>
       <?php } ?>
       </body>

</html>