<?php
include 'koneksi.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Login</title>
     <header id="header">
          <nav>
               <div class="links">
                    <ul>
                         <li>
                              <a href="index.php" class="nav-link">Home</a>
                         </li>
                         <li>
                              <a href="login.php" class="nav-link">Sign In</a>
                         </li>
                    </ul>
               </div>
          </nav>
     </header>
</head>

<body>
     <h2>Login</h2>
     <form class="" action="fungsi_login.php" method="POST">
          <label for="username">Username or Email :</label>
          <input type="text" name="username" id="username" required value=""><br>
          <label for="password">Password</label>
          <input type="password" name="password" id="password" required value=""><br>
          <button type="submit" name="login">Login</button>
     </form>
     <br>
     <a href="registrasi.php">Registration</a>
</body>

</html>