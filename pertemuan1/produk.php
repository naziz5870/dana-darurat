<?php
session_start();
include 'koneksi.php';
?>

<!DOCTYPE html>
<html lang="en">
<?php if ($_SESSION['status'] == "admin") { ?>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <title>Halaman Tambah Produk</title>
       </head>

       <body>
              <header id="header">
                     <?php
                     include('menu1.php');
                     ?>
              </header>

              <h1>Produk Penawaran Pinjaman</h1>
              <?php

              include('koneksi.php');

              $query = mysqli_query($conn, 'SELECT * FROM paket_pinjaman');
              $result = mysqli_fetch_all($query, MYSQLI_ASSOC);

              ?>
              <a href="tambah_paket.php">TAMBAH PAKET PINJAMAN</a>
              <?php foreach ($result as $result) : ?>
                     <div class="card-body">
                            <h5 class="card-title text-center"><?php echo $result['nama_paket'] ?></h5>
                            <h4 class="card-title text-center"><strong>Rp. </strong><?php echo $result['jumlah_pinjaman'] ?></h4>
                            <p class="card-text"><strong>Bunga </strong><?php echo $result['bunga_paket'] ?></p>
                            <p class="card-text"><strong>Cicilan </strong><?php echo $result['cicilan_paket'] ?></p>
                            <a href="edit_paket.php?id_paket=<?php echo $result['id_paket'] ?>">Edit</a>
                            <a href="hapus_paket.php?id_paket=<?php echo $result['id_paket'] ?>">Hapus</a>
                     <?php endforeach; ?>
       </body>
<?php } ?>


</html>